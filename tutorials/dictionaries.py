my_dict = {
    'cat': 'кошка',
    'dog': 'собака',
    'bird': 'птица'
}
new_dict = dict.fromkeys(my_dict)
print(new_dict)

new_dict = dict.fromkeys([12, 3, 4], 'number')
print(new_dict)

my_dict.update({'mouse': 'мышь'})
print(my_dict)
my_dict.update({'mouse': 'мышка'})
print(my_dict)

print(my_dict.items())
print(my_dict.popitem())
print(my_dict)

print(my_dict.keys())

print('\n')
print(12_34_56 * 2)

print('\n')
print('hello world how are you'.replace(' ', ''))

print('\n')
print(90 >> 4)

print('\n')
print(bin(19), oct(19), hex(19))
print(int('10011', 2), int('0b10011', 2))
print(0.25.as_integer_ratio())
print(float.as_integer_ratio(1.2))

print('\n')
print('Hello {} and {} and {}'.format('cat', 'dog', 'cow'))
print(format(1000.5368, '~>+15,.2f'))   # ~ - заполнитель
                                        # > - выравнивание
                                        # + - знак
                                        # 15 - размер итоговой строки в символах
                                        # , - разделитель десятков
                                        # .2 - точность, количество выводимых цифр после запятой
                                        # f - тип

print('\n')
print([char1 + char2 for char1 in 'abc' for char2 in '123'])
print([value**2 for value in range(1, 11)])

print('\n')
print(list(range(1, 10, 2)))



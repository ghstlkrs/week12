def for_continue(string, multiplication=1):
    """ Выводит строку в upper case без цифр и результат перемножения всех цифр в строке """
    print(f'\nFOR_CONTINUE --- Given string: [{string}]')

    for char in string:
        if char.isdigit():
            continue
        print(char.upper(), end='')

    for char in string:
        if char == '0' or not char.isdigit():
            continue
        multiplication *= int(char)

    print(f'\nMultiplication of all digits = {multiplication}')


def for_else(string):
    """
    Выводит букву 3 раза через сепаратор sep, если она в lower case. Если попадется "*", то цикл оборвется
    """
    print(f'\n\nFOR_ELSE --- Given string: [{string}]')

    for char in string:
        if char.islower():
            print(char, char, char, sep='|')
        if char == '*':
            print("There's * symbol - fail")
            break
    else:
        print("There's no * symbol - success")


# Вызовы
for_continue('1h2002e23ll60o 9w04o5rl87d')
for_else('h34-EllO 12wOrld')

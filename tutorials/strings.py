""" Опыт работы со строками """


def reverse_words(string, conc_str=' ', replace_space=False):
    """
    Выводит строку, где каждое слово перевернуто, при этом порядок слов сохранен.
    """
    print(f'\nREVERSE_WORDS --- Given string: [{string}]')

    valid_str = string
    if replace_space:
        # Причесать строку
        while '  ' in valid_str:
            valid_str = valid_str.replace('  ', ' ')
        valid_str = valid_str.strip()
    valid_list = valid_str.split(' ')



    # Преобразование
    result_list = []
    for elem in valid_list:
        result_list.append(elem[::-1])

    # Возврат результата
    return conc_str.join(result_list)


print(reverse_words('    hello   my         friend           ', replace_space=True))
print(reverse_words('olleh ym dneirf '))
print(reverse_words(' '))
print(reverse_words(''))
print(reverse_words('_____ 12345 _., 0  '))

# Рефакторинг
# Тестирование
# Новый функционал
# Тестирование
# Готово!

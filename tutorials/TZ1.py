import re


def kwargs_sum(is_high_level = True, **kwargs):
    """
    Находит среди параметров начинающиеся со слова sum и считает сумму их значений.

    :param kwargs: Принимает сколько угодно параметров.
    :return: Возвращает все найденные параметры и сумму значений.
    """

    result_sum = 0
    result_args = {}
    error_args = {}

    if kwargs:
        for elem, value in kwargs.items():
            if elem.startswith('sum'):
                # Добавляяем в словарь всех найденных значений
                result_args.update({elem: value})
                if isinstance(value, str):
                    # Меняем запятые на точки для определения вероятного float числа
                    value_copy = value.replace(',', '.')
                    if value_copy.replace('.', '', 1).replace('-', '', 1).isdigit() or \
                            re.match(r'^(-|)\d+(\.?\d+)*e(\+|-|)\d+$', value.lower()):
                        result_sum += float(value_copy)
                    else:
                        print(f'Переменная [{value}] c ключом "{elem}" не является числом\n')
                        result_args[elem] = 'Не является числом'
                elif isinstance(value, (int, float)):
                    result_sum += value
                else:
                    if isinstance(value, dict):
                        result_args_sub, result_sum_sub, error_args_sub = kwargs_sum(is_high_level=False, **value)
                        result_sum += result_sum_sub
                        result_args = {**result_args, **result_args_sub}
                        error_args = {**error_args, **error_args_sub}
                    else:
                        error_args[elem] = value

            else:
                error_args[elem] = value
    else:
        print('Не было получено ни одного параметра')

    if error_args and is_high_level:
        print('have_error args', len(error_args))

    return result_args, result_sum, error_args


dict_test = {}

for i in range(2):
    dict_test[f'bum_{i}'] = i

for i in range(2):
    dict_test[f'sum_{i}'] = f'-{i}.113'

    new_dict = {}
    for l in range(2):
        new_dict[f'sum_{i}'] = f'-{i}.001'

    for l in range(2):
        new_dict[f'b_{i}'] = f'-{i}.001'

    dict_test[f'sum_{i}_L'] = new_dict

print(dict_test)

result_args, result_sum, error_args = kwargs_sum(**dict_test)

print(result_sum)

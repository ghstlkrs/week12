first_set = {3, 4, 5, 6, 7}
print(type(first_set))
second_set = {1, 2, 3, 4, 5}

# Возвращает True, если в рассматриваемом множестве не найдено элементов, присутствующих в указанном объекте.
# Множества не пересекаются тогда и только тогда, когда их пересечение пусто.
print(first_set.isdisjoint(second_set))

print(first_set.issubset(first_set))

print(first_set | second_set)

print(first_set.intersection(second_set))

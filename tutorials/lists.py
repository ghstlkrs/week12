def digit_list(string):
    """
    Создает список из всех целых чисел в переданной строке
    """
    print(f'\nDIGIT_LIST --- Given string: [{string}]')

    numbers_list = []
    i = 0

    while i < len(string):
        if string[i].isdigit() or (string[i] == '-' and i != len(string) - 1 and string[i + 1].isdigit()):
            numbers_list.append(string[i])
            i += 1
            while i < len(string) and string[i].isdigit():
                numbers_list[-1] += string[i]
                i += 1
        else:
            i += 1

    for i in range(len(numbers_list)):
        numbers_list[i] = int(numbers_list[i])

    return numbers_list


print(digit_list('23k555k-7k-00k091-20-3 k45 = 3-'))
print(digit_list('23k555k-----------7k-0-0k09----1-20-3 k45 = 3   '))
print(digit_list(''))
print(digit_list('09020'))
print(digit_list('--2-543-а++=2+2445 3245+23'))



